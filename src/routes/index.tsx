import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import Auth from '../screens/Auth';
const AuthSwitch = createSwitchNavigator({
  Auth,
});

export default createAppContainer(AuthSwitch);
