import React, { useState, useEffect } from 'react';
import { Container, LoginText } from './styles';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store/ducks/state';
import { fetchRepositories } from '../../store/ducks/repositories';
export default function Auth(props) {
  const respositores = useSelector((state: RootState) => state.repository);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchRepositories());
  }, []);
  console.log(respositores);
  const [auth, setAuth] = useState(null);
  return (
    <Container>
      <LoginText>Ok</LoginText>
      {respositores.data.map(repo => (
        <LoginText key={repo.id}>{repo.login}</LoginText>
      ))}
    </Container>
  );
}
