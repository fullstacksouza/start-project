import { combineReducers, Reducer } from 'redux';
import { repositoryReducer } from './repositories';

export const rootReducer = combineReducers({
  repository: repositoryReducer,
});
