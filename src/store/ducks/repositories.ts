import createReducer from '../../util/createReducer';
import { Reducer } from 'redux';
import { RootState } from './state';
import { createSelector } from 'reselect';
import { ThunkAction } from 'redux-thunk';
import api from '../../services/api';
export enum RepositoriesTypes {
  LIST_START = '@repositories/LIST_START',
  LIST_SUCCCES = '@repositories/LIST_SUCCCES',
  LIST_FAILURE = '@repositories/LIST_FAILURE',
  GET_START = '@repositories/GET_START',
  GET_SUCCCES = '@repositories/GET_SUCCCES',
  GET_FAILURE = '@repositories/GET_FAILURE',
  CREATE_START = '@repositories/CREATE_START',
  CREATE_SUCCCES = '@repositories/CREATE_SUCCCES',
  CREATE_FAILURE = '@repositories/CREATE_FAILURE',
  UPDATE_START = '@repositories/UPDATE_START',
  UPDATE_SUCCCES = '@repositories/UPDATE_SUCCCES',
  UPDATE_FAILURE = '@repositories/UPDATE_FAILURE',
  DELETE_START = '@repositories/DELETE_START',
  DELETE_SUCCCES = '@repositories/DELETE_SUCCCES',
  DELETE_FAILURE = '@repositories/DELETE_FAILURE',
}

export interface Repository {
  id?: number;
  name: string;
  login: string;
}

export interface RepositoryState {
  data: Repository[];
  loading: LoadingSection;
  error: any;
}

export interface LoadingSection {
  'loading.list': boolean;
  'loading.create': boolean;
  'loading.update': boolean;
  'loading.get': boolean;
  'loading.delete': boolean;
}

export const INITIAL_STATE: RepositoryState = {
  data: [],
  loading: {
    'loading.list': false,
    'loading.create': false,
    'loading.update': false,
    'loading.get': false,
    'loading.delete': false,
  },
  error: false,
};

export type Actions = {
  ListStart: { type: RepositoriesTypes.LIST_START };
  ListSuccess: { type: RepositoriesTypes.LIST_SUCCCES; payload: Repository[] };
  ListFailure: { type: RepositoriesTypes.LIST_FAILURE; payload: any };

  CreateStart: { type: RepositoriesTypes.CREATE_START };
  CreateSuccess: {
    type: RepositoriesTypes.CREATE_SUCCCES;
    payload: Repository;
  };
  CreateFailure: { type: RepositoriesTypes.CREATE_FAILURE; payload: any };

  UpdateStart: { type: RepositoriesTypes.UPDATE_START };
  UpdateSuccess: {
    type: RepositoriesTypes.UPDATE_SUCCCES;
    payload: Repository;
  };
  UpdateFailure: { type: RepositoriesTypes.UPDATE_FAILURE; payload: any };

  DeleteStart: { type: RepositoriesTypes.DELETE_START };
  DeleteSuccess: {
    type: RepositoriesTypes.DELETE_SUCCCES;
    payload: Repository;
  };
  DeleteFailure: { type: RepositoriesTypes.DELETE_FAILURE; payload: any };

  GetStart: { type: RepositoriesTypes.GET_START };
  GetSuccess: { type: RepositoriesTypes.GET_SUCCCES; payload: Repository };
  GetFailure: { type: RepositoriesTypes.GET_FAILURE; payload: any };
};

export const repositoryReducer: Reducer<RepositoryState> = createReducer(
  INITIAL_STATE,
  {
    [RepositoriesTypes.LIST_START](state: RepositoryState) {
      state.loading['loading.list'] = true;
      return state;
    },
    [RepositoriesTypes.LIST_SUCCCES](
      state: RepositoryState,
      action: Actions['ListSuccess']
    ) {
      state.loading['loading.list'] = false;
      state.data = action.payload;
      return state;
    },
  }
);

//Selectors
const mainSelector = (state: RootState) => state.repository;

export const getRepositoryList = createSelector(
  mainSelector,
  (state: RepositoryState) => ({
    list: state.data || [],
    loading: state.loading['loading.list'],
    error: state.error,
  })
);

export function fetchRepositories(): ThunkAction<
  Promise<void>,
  RootState,
  any,
  any
> {
  return async (dispatch, getstate) => {
    dispatch({ type: RepositoriesTypes.LIST_START } as Actions['ListStart']);
    return api
      .get('/users')
      .then(response => {
        dispatch({
          type: RepositoriesTypes.LIST_SUCCCES,
          payload: response.data,
        });
      })
      .catch(err => {
        dispatch({ type: RepositoriesTypes.LIST_FAILURE });
      });
  };
}
