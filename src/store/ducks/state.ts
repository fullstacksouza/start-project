import { RepositoryState } from './repositories';

export interface RootState {
  repository: RepositoryState;
}

export declare namespace RootState {}
